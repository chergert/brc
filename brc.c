/* brc.c
 *
 * Copyright 2023 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "brc.h"

typedef struct _brc_global_t
{
  GRWLock rwlock;
  GHashTable *threads;
  guint last_tid;
} brc_global_t;

typedef struct _brc_queued_object_t
{
  GList link;
  brc_biased_t *biased;
  brc_shared_t *shared;
} brc_queued_object_t;

static void brc_thread_unregister (gpointer data);

static brc_global_t brc_global;
GPrivate brc_thread_key = G_PRIVATE_INIT (brc_thread_unregister);

static void
brc_refcount_explicit_merge (brc_biased_t *biased,
                             brc_shared_t *shared)
{
  brc_shared_t old;
  brc_shared_t new;

  do
    {
      old = *shared;
      new = old;
      new.counter += biased->counter;
      new.merged = TRUE;
    }
  while (!atomic_compare_exchange_strong (shared, &old, new));

  if (new.counter == 0)
    brc_refcount_finalize (biased, shared);
  else
    biased->tid = 0;
}

static gboolean
brc_thread_explicit_merge_cb (brc_thread_t *thread)
{
  brc_queued_object_t *queued_object;
  GQueue stolen;

  g_mutex_lock (&thread->mutex);
  stolen = thread->objects;
  memset (&thread->objects, 0, sizeof thread->objects);
  g_mutex_unlock (&thread->mutex);

  while ((queued_object = g_queue_peek_head (&stolen)))
    {
      g_queue_unlink (&stolen, &queued_object->link);
      brc_refcount_explicit_merge (queued_object->biased, queued_object->shared);
      g_free (queued_object);
    }

  return G_SOURCE_CONTINUE;
}

typedef struct _brc_merge_source_t
{
  GSource gsource;
  brc_thread_t *thread;
} brc_merge_source_t;

static gboolean
brc_merge_source_check (GSource *source)
{
  brc_merge_source_t *merge = (brc_merge_source_t *)source;
  gboolean ret;

  g_mutex_lock (&merge->thread->mutex);
  ret = merge->thread->objects.length > 0;
  g_mutex_unlock (&merge->thread->mutex);

  return ret;
}

static gboolean
brc_merge_source_dispatch (GSource     *source,
                           GSourceFunc  callback,
                           gpointer     callback_data)
{
  if (callback != NULL)
    return callback (callback_data);
  return G_SOURCE_CONTINUE;
}

static GSourceFuncs merge_source_funcs = {
  .check = brc_merge_source_check,
  .dispatch = brc_merge_source_dispatch,
};

static brc_thread_t *
brc_global_get_next_thread (void)
{
  GMainContext *main_context;
  brc_thread_t *thread;
  char source_name[32];
  gboolean cycle = FALSE;
  guint tid;

  main_context = g_main_context_ref_thread_default ();
  thread = g_new0 (brc_thread_t, 1);

  g_rw_lock_writer_lock (&brc_global.rwlock);

  if (brc_global.threads == NULL)
    brc_global.threads = g_hash_table_new (NULL, NULL);

  do
    {
      tid = ++brc_global.last_tid % 0x3FFFF;

      if (tid == 0)
        {
          if (cycle)
            g_error ("Failed to register thread, too many threads");
          cycle = TRUE;
          tid = 1;
        }
    }
  while (g_hash_table_contains (brc_global.threads, GUINT_TO_POINTER (tid)));

  g_assert (tid > 0);
  g_assert (tid <= 0x3FFFF);

  g_mutex_init (&thread->mutex);
  thread->tid = tid;

  g_snprintf (source_name, sizeof source_name, "[brc-explicit-merge %u]", tid);
  thread->explicit_merge_source = g_source_new (&merge_source_funcs, sizeof (brc_merge_source_t));
  ((brc_merge_source_t *)thread->explicit_merge_source)->thread = thread;
  g_source_set_name (thread->explicit_merge_source, source_name);
  g_source_set_priority (thread->explicit_merge_source, G_PRIORITY_LOW);
  g_source_set_callback (thread->explicit_merge_source,
                         (GSourceFunc)brc_thread_explicit_merge_cb,
                         thread,
                         NULL);

  g_private_set (&brc_thread_key, thread);
  g_hash_table_insert (brc_global.threads, GUINT_TO_POINTER (tid), thread);
  g_rw_lock_writer_unlock (&brc_global.rwlock);

  g_source_attach (thread->explicit_merge_source, main_context);
  g_main_context_unref (main_context);

  return thread;
}

brc_thread_t *
brc_thread_register (void)
{
  brc_thread_t *thread = g_private_get (&brc_thread_key);

  if (thread != NULL)
    return thread;

  return brc_global_get_next_thread ();
}

static void
brc_thread_unregister (gpointer data)
{
}

void
brc_refcount_queue (brc_biased_t *biased,
                    brc_shared_t *shared)
{
  brc_thread_t *owner_thread;
  brc_queued_object_t *queued_object;

  queued_object = g_new0 (brc_queued_object_t, 1);
  queued_object->link.data = queued_object;
  queued_object->biased = biased;
  queued_object->shared = shared;

  g_rw_lock_reader_lock (&brc_global.rwlock);

  if (biased->tid > 0 &&
      (owner_thread = g_hash_table_lookup (brc_global.threads, GUINT_TO_POINTER (biased->tid))))
    {
      g_mutex_lock (&owner_thread->mutex);
      g_rw_lock_reader_unlock (&brc_global.rwlock);
      g_queue_push_tail_link (&owner_thread->objects, &queued_object->link);
      if (!owner_thread->did_wakeup)
        {
          owner_thread->did_wakeup = TRUE;
          g_main_context_wakeup (g_source_get_context (owner_thread->explicit_merge_source));
        }
      g_mutex_unlock (&owner_thread->mutex);

      return;
    }

  g_rw_lock_reader_unlock (&brc_global.rwlock);
  brc_refcount_explicit_merge (biased, shared);
}
