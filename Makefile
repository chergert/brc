all: test

test: brc.c brc.h main.c
	$(CC) -std=gnu11 -o $@ -Wall $(shell pkg-config --cflags --libs glib-2.0) -Wpedantic main.c brc.c -ggdb -O0

clean:
	rm -f test
