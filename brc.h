/*
 * brc.h
 *
 * Copyright 2023 Christian Hergert <chergert@gnome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <stdatomic.h>

#include <glib.h>

G_BEGIN_DECLS

typedef union _brc_biased_t
{
  unsigned raw;
  struct {
    unsigned tid : 18;
    int counter : 14;
  };
} brc_biased_t;

typedef union _brc_shared_t
{
  unsigned raw;
  struct {
    int counter : 14;
    unsigned merged : 1;
    unsigned queued : 1;
    int reserved : 16;
  };
} brc_shared_t;

typedef struct _brc_thread_t
{
  GMutex mutex;
  unsigned tid : 18;
  unsigned reserved : 13;
  unsigned did_wakeup : 1;
  GQueue objects;
  GSource *explicit_merge_source;
} brc_thread_t;

brc_thread_t *brc_thread_register (void);

extern GPrivate brc_thread_key;
extern void brc_refcount_finalize (brc_biased_t *biased,
                                   brc_shared_t *shared);

static brc_thread_t *
brc_get_thread (void)
{
  brc_thread_t *thread = g_private_get (&brc_thread_key);

  if G_LIKELY (thread != NULL)
    return thread;
  else
    return brc_thread_register ();
}

static inline guint
brc_get_thread_id (void)
{
  return brc_get_thread ()->tid;
}

static inline void
brc_refcount_init (brc_biased_t *biased,
                   brc_shared_t *shared)
{
  biased->tid = brc_get_thread_id ();
  biased->counter = 1;
  shared->raw = 0;
}

static inline void
brc_refcount_inc_fast (brc_biased_t *biased)
{
  biased->counter++;
}

static inline void
brc_refcount_inc_slow (brc_shared_t *shared)
{
  brc_shared_t old;
  brc_shared_t new;

  do
    {
      old = *shared;
      new = old;
      new.counter += 1;
    }
  while (!atomic_compare_exchange_strong (shared, &old, new));
}

static inline void
brc_refcount_inc (brc_biased_t *biased,
                  brc_shared_t *shared)
{
  if (biased->tid == brc_get_thread_id ())
    brc_refcount_inc_fast (biased);
  else
    brc_refcount_inc_slow (shared);
}

static inline gboolean
brc_refcount_dec_fast (brc_biased_t *biased,
                       brc_shared_t *shared)
{
  brc_shared_t old;
  brc_shared_t new;

  g_assert (biased->tid == brc_get_thread_id ());

  biased->counter--;

  if (biased->counter > 0)
    return FALSE;

  do
    {
      old = *shared;
      new = old;
      new.merged = TRUE;
    }
  while (!atomic_compare_exchange_strong (shared, &old, new));

  if (new.counter == 0)
    return TRUE;

  biased->tid = 0;

  return FALSE;
}

void brc_refcount_queue (brc_biased_t *biased,
                         brc_shared_t *shared);

static inline gboolean
brc_refcount_dec_slow (brc_biased_t *biased,
                       brc_shared_t *shared)
{
  brc_shared_t old;
  brc_shared_t new;

  do
    {
      old = *shared;
      new = old;
      new.counter -= 1;
      if (new.counter < 0)
        new.queued = TRUE;
    }
  while (!atomic_compare_exchange_strong (shared, &old, new));

  if (old.queued != new.queued)
    {
      brc_refcount_queue (biased, shared);
      return FALSE;
    }

  if (new.merged == TRUE && new.counter == 0)
    return TRUE;

  return FALSE;
}

static inline gboolean
brc_refcount_dec (brc_biased_t *biased,
                  brc_shared_t *shared)
{
  if (biased->tid == brc_get_thread_id ())
    return brc_refcount_dec_fast (biased, shared);
  else
    return brc_refcount_dec_slow (biased, shared);
}

G_END_DECLS
