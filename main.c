#include "brc.h"

static GMainLoop *main_loop;
static GMutex mutex;
static GCond cond;
static guint wait_count;

typedef struct _Object
{
  brc_biased_t biased;
  brc_shared_t shared;
  guint finalized : 1;
} Object;

void
brc_refcount_finalize (brc_biased_t *biased,
                       brc_shared_t *shared)
{
  Object *obj = (Object *)biased;

  g_print ("Finalize\n");

  /* TODO: BRC requires knowing how to finalize your type, so we
   * need a system to be able to get type info for that or just
   * hard code it to every object type. The downside there is
   * duplication of precious pthread keys.
   */

  obj->finalized = TRUE;

  g_main_loop_quit (main_loop);
}

static Object *
object_ref (Object *obj)
{
  brc_refcount_inc (&obj->biased, &obj->shared);
  return obj;
}

static gboolean
object_unref (Object *obj)
{
  return brc_refcount_dec (&obj->biased, &obj->shared);
}

static gpointer
gauntlet_thread (gpointer data)
{
  Object *obj = data;

  g_mutex_lock (&mutex);
  g_atomic_int_inc (&wait_count);
  g_cond_wait (&cond, &mutex);
  g_mutex_unlock (&mutex);

  for (guint i = 0; i < 20; i++)
    object_ref (obj);

  for (guint i = 0; i < 20; i++)
    g_assert_false (object_unref (obj));

  g_assert_false (obj->finalized);

  if (object_unref (obj) == TRUE)
    brc_refcount_finalize (&obj->biased, &obj->shared);

  return NULL;
}

int
main (int argc,
      char *argv[])
{
  Object *obj = g_new0 (Object, 1);
  GThread *threads[20];

  main_loop = g_main_loop_new (NULL, FALSE);

#if 0
  brc_refcount_init (&obj->biased, &obj->shared);
  g_assert_true (brc_refcount_dec (&obj->biased, &obj->shared));

  brc_refcount_init (&obj->biased, &obj->shared);
  brc_refcount_inc (&obj->biased, &obj->shared);
  g_assert_false (brc_refcount_dec (&obj->biased, &obj->shared));
  g_assert_true (brc_refcount_dec (&obj->biased, &obj->shared));
#endif

  memset (obj, 0, sizeof *obj);
  brc_refcount_init (&obj->biased, &obj->shared);
  g_assert_cmpint (obj->biased.counter, ==, 1);

  g_mutex_lock (&mutex);
  for (guint i = 0; i < G_N_ELEMENTS (threads); i++)
    threads[i] = g_thread_new ("brc-gauntlet", gauntlet_thread, object_ref (obj));
  g_assert_false (object_unref (obj));
  g_assert_false (obj->finalized);
  g_assert_cmpint (wait_count, ==, 0);
  while (g_atomic_int_get (&wait_count) < G_N_ELEMENTS (threads))
    {
      g_mutex_unlock (&mutex);
      g_usleep (G_USEC_PER_SEC / 1000);
      g_mutex_lock (&mutex);
    }
  g_idle_add_once ((GSourceOnceFunc)g_mutex_unlock, &mutex);
  g_cond_broadcast (&cond);
  g_main_loop_run (main_loop);

  for (guint i = 0; i < G_N_ELEMENTS (threads); i++)
    g_thread_join (threads[i]);

  g_main_loop_unref (main_loop);
  g_free (obj);

  return 0;
}
